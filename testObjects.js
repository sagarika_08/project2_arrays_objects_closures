const inputData = require('./inputData')
const objects = require('./objects')
//console.log(inputData.testObject)


// test keys
console.log('\n\n\nKEYS\n')
const keyResult = objects.keys(inputData.testObject)
console.log(keyResult)

//test values
console.log('\n\n\nVALUES\n')
const valueResult = objects.values(inputData.testObject)
console.log(valueResult)



//test mapObject
console.log('\n\n\n MAP_OBJECT\n')
const testCallback=function(val){
    return (val + val)
}
console.log(objects.mapObject(inputData.testObject,testCallback))



//test pairs
console.log("\n\n\n PAIRS\n")
const resultPairs = objects.pairs(inputData.testObject);
console.log(resultPairs)



//test invert
console.log('\n\n\n INVERT\n')
const resultInvert =objects.invert(inputData.testObject)
console.log(resultInvert)



//test defaults
console.log('\n\n\n DEFAULT')
const defaultProps = { name: 'John',Country : 'India'}; 
const resultDefaultPairs = objects.defaults(inputData.testObject,defaultProps)
console.log(resultDefaultPairs)