const inputData = require('./inputData')
const arrays = require('./arrays')



// test each()
console.log('\n\n\nEACH FUNCTION')
function testEach(item, index) {
    console.log(item)
}
arrays.each(inputData.items, testEach)





// test map()
console.log('\n\n\nMAP FUNCTION')
function testMap(item) { return (item * 3) }
let mapResult = arrays.map(inputData.items, testMap)
if (Array.isArray(mapResult)==true && mapResult.length > 0)
    console.log('Array ' + '[' + mapResult.join(',') + ']')
else console.log(mapResult)



// test reduce 

console.log('\n\n\nREDUCE FUNCTION\n')
function testReduce(accumulator, currentValue) {
    return (accumulator + currentValue)
}
let resultReduce = arrays.reduce(inputData.items, testReduce)
console.log(resultReduce)







//test find

console.log('\n\n\nFIND FUNCTION\n')
function testFind(item) {
    return ((item > 4) ? true : false);
}
let resultFind = arrays.find(inputData.items, testFind)
console.log(resultFind)





// test filter()

console.log('\n\n\nFILTER FUNCTION\n')
function testFilter(item) {
    return ( (item > 4) ? true : false)
}
let filterResult = arrays.filter(inputData.items, testFilter)
if (Array.isArray(filterResult)==true && filterResult.length > 0)
    console.log('Array ' + '[' + filterResult.join(',') + ']')
else
    console.log(filterResult)


//test flatten()


const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
console.log('\n\n\nFLATTEN \n')
console.log(arrays.flatten(nestedArray))