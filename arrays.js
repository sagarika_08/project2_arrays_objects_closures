function each(elements, cb) {
    if (((typeof elements) != "undefined") && (elements.length > 0 == true) && ((typeof cb) != "undefined") && Array.isArray(elements) == true) {
        console.log("hi")
        for (let i = 0; i < elements.length; i++)
            cb(elements[i], i)
    }
    else if ((typeof elements == "undefined") || (Array.isArray(elements) == false))
        console.log("Error: array is not defined")
    else if ((typeof cb) == "undefined")
        console.log("Call back function is not defined")
    else if ((elements.length <= 0) == true)
        console.log([])
}


function map(elements, cb) {
    let map_Array = []
    if (((typeof elements) != "undefined") && (elements.length > 0 == true) && ((typeof cb) != "undefined") && Array.isArray(elements) == true) {
        for (let i = 0; i < elements.length; i++)
            map_Array.push(cb(elements[i]));
    }
    if ((map_Array.length > 0) == true) return map_Array
    else if (!(Array.isArray(elements).length <= 0) && (cb))
        return ([])
    else if ((Array.isArray(elements) == true) && (typeof cb == "undefined"))
        return ("Call back function is not defined")
    else if ((Array.isArray(elements) == true) && (typeof cb != "undefined"))
        return ("Error: array is not defined")
}

function reduce(elements, cb, startingValue) {
    if (((typeof elements) != "undefined") && (elements.length > 0 == true) && ((typeof cb) != "undefined") && Array.isArray(elements) == true) {
        let i;
        startingValue ? i = 0 : (i = 1, startingValue = elements[0])
        for (i; i < elements.length; i++) {
            resultReduce = cb(startingValue, elements[i])
            startingValue = resultReduce;
        }
        return resultReduce;
    }
    else
        return ('Error: Array is note defined')
}

function find(elements, cb) {
    if (((typeof elements) != "undefined") && (elements.length > 0 == true) && ((typeof cb) != "undefined") && Array.isArray(elements) == true) {
        let flag = 0;
        let i = 0
        for (i; i < elements.length; i++) {
            if (cb(elements[i])) {
                flag = 1
                break
            }
        }
        return ((flag === 1) ? elements[i] : 'undefined')
    }
    else
        return ('undefined')
}


function filter(elements, cb) {
    let filterArray = []
    if (((typeof elements) != "undefined") && (elements.length > 0 == true) && ((typeof cb) != "undefined") && Array.isArray(elements) == true) {
        for (let i = 0; i < elements.length; i++) {
            if (cb(elements[i]))
                filterArray.push(elements[i])
        }
        return (filterArray)
    }
    else if (!(Array.isArray(elements).length <= 0) && (typeof cb != "undefined"))
        return ([])
    else if ((Array.isArray(elements) == true) && (typeof cb == "undefined"))
        return ("Call back function is not defined")
    else if (Array.isArray(elements) == false || (typeof cb != "undefined"))
        return ("Error: array is not defined")
}


function flatten(elements, resultFlatten = []) {
    if (elements === undefined) return [];
    for (let i = 0; i < elements.length; i++) {
        if (!Array.isArray(elements[i]))
            resultFlatten.push(elements[i])
        else
            flatten(elements[i], resultFlatten)
    }
    return resultFlatten
}



module.exports = {
    each,
    map,
    reduce,
    find,
    filter,
    flatten
}