function counterFactory() {
  let count = 0
  return {
    increment: function () { count++; return count; },
    decrement: function () { count--; return count }

  };
}

function limitFunctionCallCount(cb, n) {
  return (function (arg) {
      let i = 1
      while (i <= n) {
        cb(arg)
        i++
      }
    })
    }
    


    function cacheFunction(cb) {
      let cache = {};
      return(function(arg){
          if(arg in cache)  return cache[arg];
          else{
              let resultCache = cb(arg);
              cache[arg] = resultCache;
              return resultCache;
          }
      })
  }
module.exports = {
  counterFactory,
  limitFunctionCallCount,
  cacheFunction
}