
function keys(obj) {
    let keyArray = []
    if (obj!="undefined") {
        for (let key in obj) {
            keyArray.push(key)
        }
    }
    return keyArray
}


function values(obj) {
    let valueArray = [];
    if (obj!="undefined") {
        for (let key in obj) {
            valueArray.push(obj[key])
        }
    }
    return valueArray
}

function mapObject(obj, cb) {
    if ((obj!="undefined") && (cb!="undefined")) {
        let resultMapObject = {}
        for (let key in obj) {
            resultMapObject[key] = cb(obj[key])
        }
        return resultMapObject
    }
    else
        return ("undefined")
}

function pairs(obj) {
    if (obj!="undefined") {
        let resultPairs = []
        for (let key in obj) {
            let arr = []
            arr.push(key, obj[key])
            resultPairs.push(arr)
        }
        return resultPairs
    }
    else
        return ("undefined")
}


function invert(obj) {
    let resultInvert = {}
    if (obj!="undefined") {
        for (let key in obj) {
            resultInvert[obj[key]] = key
        }
    }
    return (resultInvert)
}


function defaults(obj, defaultProps) {
    if (obj!="undefined" && defaultProps!="undefined") {
        let flag
        for (let defKey in defaultProps) {
            flag = 0
            for (let objKey in obj) {
                if (defKey === objKey) {
                    flag = 1
                    break;
                }
            }
            if (flag === 0) obj[defKey] = defaultProps[defKey]
        }
        return obj

    }
    else if (obj=="undefined") return (defaultProps)
    else if (defaultProps=="undefined") return (obj)
    else return({})

}

module.exports = {
    keys,
    values,
    mapObject,
    pairs,
    invert,
    defaults
}