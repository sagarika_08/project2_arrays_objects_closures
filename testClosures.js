const closure = require('./closures')



//test closures
console.log('\n\n\ncounterFactory\n')
console.log(closure.counterFactory().increment(),closure.counterFactory().decrement())





//test limitFunctionCallCount
console.log('\n\n\nlimitFunctionCallCount\n')
const limitCallback = function(i){
    console.log(" hello " )
}
const resultLimit= closure.limitFunctionCallCount(limitCallback,7)
resultLimit(8)



//cacheFunction
console.log('\n\n\ncacheFunction\n')
const test1= function(n){
   // console.log('in callback')
    return n*n*n;
 }
 const resultCacheFunction = closure.cacheFunction(test1);
 console.log(resultCacheFunction(5));
 console.log(resultCacheFunction(70));
 console.log(resultCacheFunction(8));
 console.log(resultCacheFunction(5));
 console.log(resultCacheFunction(70));
